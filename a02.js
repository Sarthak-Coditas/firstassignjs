"use strict"; //to avoid mistakes 

//declaring a variable
let firstName = "Sarthak";
//using a variable
console.log(firstName); 

//we can also change the value of variable by keeping variable name the same
firstName = "Sahil";

console.log(firstName);