// Rules for naming variable
//1. you cannot start with number
//2. you cannot give space
//3/ you can only use _ and $ sybmbol

//1
//invalid
// let 1value = 4;

//valid
let value1 = 4;
console.log(value1);

//2
//invalid
// let value 1 = 4;

//valid
let value2 = 4;
console.log(value2);

//3
//invalid
//let %firstName = 4;

//valid
let value3 = 4;
console.log(value3);