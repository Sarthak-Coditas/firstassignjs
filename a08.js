// type of operator

// how to understand type of operator
let age = 22;
let firstName = "Sarthak";
console.log(typeof firstName);

// converting number to string 
console.log(typeof (age+""));

// converting string to number
let newStr = +'72';
console.log(typeof newStr);