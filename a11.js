//undefined
let firstName;
console.log(typeof firstName);
firstName = "Sarthak";
console.log(typeof firstName)

//null
let myNum = null;
console.log(typeof myNum);
myNum = "Sarthug";
console.log(typeof myNum);

//BigInt

let newNum = BigInt(123322232332342342432);
console.log(newNum);

// 12 is also a bigint number just we have to specify n at the end of the number
