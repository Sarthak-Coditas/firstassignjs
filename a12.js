//booleans and comparison operators

num1 =5;
num2 =5;

console.log(num1 >= num2);

//== and ===

//==
console.log(num1 == num2);

//===
num3 = 4;
num4 = "5";
//different datatypes names are not allowed for === they are only applicable for ==
console.log(num3 == num4);

//!=
console.log(num1 != num2);

//!==
//concepts of === and !== are almost the same
console.log(num3 !==num4);
