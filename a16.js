// and or operator
let firstName = "Sarthak";
let age = 23;

//&&
if(firstName[1]==="H" && age>18)
{
    console.log("You're fine");
}
else
{
    console.log("Enter correct details");
}

// ||
if(firstName[1]==="H" || age>18)
{
    console.log("You're fine");
}
else
{
    console.log("Enter correct details");
}
