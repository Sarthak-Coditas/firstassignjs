// Game of winning 
// winning number is 78

let num = 78;
let userGuess = +prompt("Guess the winning number");

if(userGuess === num)
{
    console.log("You have won and guessed the correct number");
}
else
{
    if(userGuess > num)
    {
        console.log("Entered number is too high");
    }
    else
    {
        console.log("Entered number is too low");
    }
}