 //array

 let string = ["Sarthak","Soham","Sahil"];
 console.log(string);

// the variable names can be changed

let fruits = ["mango","apple","grapes"];
console.log(fruits);
fruits[1]= "watermelon";
console.log(fruits);

console.log(typeof fruits);
console.log(Array.isArray(fruits));