// push
let fruits = ["mango","apple","grapes"];
console.log(fruits);
fruits.push("banana");
console.log(fruits);

//pop
let fruit = ["mango","apple","grapes"];
console.log(fruit);
fruit.pop();
console.log(fruit);

//unshift
let fruitss = ["mango","apple","grapes"];
console.log(fruitss);
fruitss.unshift("banana");
console.log(fruitss);

//shift
console.log(fruitss);
fruitss.shift();
console.log(fruitss);