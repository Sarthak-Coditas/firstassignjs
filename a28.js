// primitive datatypes 
let num1 = 6;
let num2 = num1;
console.log("Value of num1 is : ",num1);
console.log("Value of num2 is : ",num2);
num1++;
console.log("Value of num1 is : ",num1);
console.log("Value of num2 is : ",num2);

//reference datatypes
let arr1 = ["item1","item2"];
let arr2 = arr1;
console.log("Value of array1 is : ",arr1);
console.log("Value of array2 is : ",arr2);
arr1.push("item3");
console.log("Value of array1 is : ",arr1);
console.log("Value of array2 is : ",arr2);
