//array destructuring
const names = ["Sarthak","Sahil","Soham"];

//let name1 = names[0];
//let name2 = names[1];
//let name3 = names[2];
//instead of using we can sum up this into a single line

let [name1, name2, name3] = names;
console.log("value of name1 : ",name1);
console.log("value of name2 : ",name2);
console.log("value of name3 : ",name3);
