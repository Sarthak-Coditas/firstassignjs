//difference between dot and bracket notation
const key = "address";
const person = {name : "Sarthak",
                age : 22, 
                gender : "Male" ,
                "person hobbies" : ["Cricket","F1"] // for using 2 words in key value double quotes are necessary
               };

//dot notation
console.log(person.name);//we can access this data
//console.log(person.person hobbies);//we can't access the data if there's a space

//bracket notation
console.log(person["person hobbies"]);//this is possible

//adding a new key value into the array
person[key] = "Vimannagar,Pune";
console.log(person);
