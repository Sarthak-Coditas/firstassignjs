//iteration of object
const person = {name : "Sarthak",
                age : 22, 
                gender : "Male" ,
                "person hobbies" : ["Cricket","F1"] // for using 2 words in key value double quotes are necessary
               };

//3 ways to iterate the object
//for in loop
//for off loop
//object.keys

//for in loop
for(let key in person)
   {
    console.log(key);
    console.log(`${key}: ${person[key]}`);
   }

//for of loop
for(let key of Object.keys(person))
   {
    console.log(person[key]);
   }

 //object.keys
 console.log(typeof (Object.keys(person))); //this will print object
 const val =  Array.isArray((Object.keys(person)));
 console.log(val);
