//spread operator

//1.In array
const arr1 = [1,2,3];
const arr2 = [4,5,6];

//const result = [...arr1,...arr2];
const result = [..."abcdef"];
console.log(result);

//2.In objects
const arr3 = {
    key1 : "value1",
    key2 : "value2",
};
const arr4 = {
    key3 : "value3",
    key4 : "value4",
};

const newResult = {...arr3, ...arr4};
console.log(newResult);