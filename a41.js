//object destructuring

const team = {
     
    teamName : "Mercedes",
    driverName : "Lewis",
    carName : "W13",
}

let {teamName,driverName,carName} = team;
console.log(team);