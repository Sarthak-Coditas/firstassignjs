//nested destructuring

const teams = [
    {dName : "Lewis", teamName : "Mercedes", wins : 103},
    {dName : "Max", teamName : "RedBull", wins : 0},
    {dName : "Leclerc", teamName : "Ferrari", wins : 5},
];

const [{dName: newName} ,user2, user3] = teams;
console.log(newName);
console.log(user2);
console.log(user3);