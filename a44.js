//function declaration
function sayHelloooo (){
    console.log("Hello there");
}
sayHelloooo();

// creating a resuable function
function sumOfTwoNums (num1 , num2){
    return num1+num2;
}

const resultOfTwoNums = sumOfTwoNums(9,8);
console.log(resultOfTwoNums); 

 //odd or even example 
 function isEven(num)
 {
    return num % 2 === 0;
 }
 console.log(isEven(6));

 //string example
 function isString(word){

    return word[1];

 }
 console.log(isString("Sarthak"));

 //array example
 function isTarget(array,target){
    for(let i = 0;i<=array.length;i++)
    {
    if(array[i]===target)
    {
        return i;
    }

    }
    return -1;
 }
 const newArray = [1,4,8,55,44,22];
 const ans = isTarget(newArray, 8);
 console.log(ans);