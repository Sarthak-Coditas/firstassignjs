//function expression

//This is function declaration 
function sayHellooo(){
    console.log("Hey hello there");

}
sayHellooo();

//This is function expression

const sayHelloo = function(){
    console.log("Hey hello there how are you.");

}
sayHelloo();

//example of function expression

const isEven = function(num){
    return num % 2 === 0;
}
console.log(isEven(5));