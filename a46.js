//arrow function

//function declaration
function sayHelloooo (){
    console.log("Hello there");
}
sayHelloooo();

//function expression

const sayHelloo = function(){
    console.log("Hey hello there how are you.");

}
sayHelloo();

//arrow functions

const sayHello = () =>
{
    console.log("Hey hello there how are you, all good?.");

}
sayHello();

//example

const isEven = (num) =>{
    return num % 2 === 0;
}
console.log(isEven(5));