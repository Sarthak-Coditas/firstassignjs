//hoisting

//function declaration
function welcome() {
    console.log("Welcome to Cod;tas");
}
welcome();

//hoisting in function declaration
welcome();
function welcome() {
    console.log("Welcome to Cod;tas");
}

//hoisting is not possible in function expression and arrow functions

console.log(newString); //output = undefined
var newString = "Hello there";
console.log(newString); //output = Hello there

//in case of let and const it will give error



