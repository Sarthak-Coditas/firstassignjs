//lexical scope

const myVar = "Value1";
function myApp()
{
    function myFunc()
    //const myVar = "Value2";
    {
        const myFunct2 = () =>
        {
            console.log("Welcome...");

        }
        myFunct2();
    }
    console.log(myVar);
    myFunc();


}
myApp();