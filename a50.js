//function scope vs block scope
 
//function scope
//only var can be accessed outside and inside the block
{
  var firstName = "Sarthak";
  console.log(firstName);
}
console.log(firstName);

//block scope
//let and const can only accessed in that particular block only
//let and const are used more than var as they can be accessed in same block
{
  let secondName = "Sahil";
  const thirdName = "Shubham";
}
console.log(secondName);
console.log(thirdName);
