//default parameters
// if a variable is not assigned any number it can declared as a default parameter
function addNum(num1 , num2=0)
{
    return num1 + num2;
}
const ans = addNum(7);
console.log(ans);

