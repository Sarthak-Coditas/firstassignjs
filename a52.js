//rest parameters
function addAll(...numbers)
{
   let total = 0;
   for(let number of numbers)
   {
    total = total + number;
   }
   return total;
}

const ans  = addAll(4,6,7,4,2,32);
console.log(ans);