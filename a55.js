//function returning function
function funct1()
{
  function funct2()
  {
    return "Hello World";
  }
  return funct2;
}

const ans = funct1();
console.log(ans());
