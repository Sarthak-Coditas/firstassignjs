//array methods
//foreach loop
const numbers = [1,4,7,9];
numbers.forEach(function(number,index)//its okay to not write index but for better practice its important
{
    console.log(`index is ${index} and number is ${number} `);

});

const users = 
[
  {firstName: "Sarthak", age:23},
  {firstName: "Sarthug", age:24},
  {firstName: "Sahil", age:25},
]

users.forEach(function(user)
{
    console.log(user.firstName);
}
)