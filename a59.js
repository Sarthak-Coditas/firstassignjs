//reduce

const numbers = [1,2,3,4,5,6];
const sum = numbers.reduce((num1,num2) =>
{
 return num1 + num2 ;
});
console.log(sum);

//example
const users = 
[
  {firstName: "Sarthak", age:23, salary:40000},
  {firstName: "Sarthug", age:24, salary:30000},
  {firstName: "Sahil", age:25 , salary:20000},
]
const total = users.reduce((totalPrice,currentSalary) =>
{
 return totalPrice + currentSalary.salary;

},20  );
console.log(total);


