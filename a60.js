//sorting

//ascending order
const numbers = [6,2,9,4];
numbers.sort((a,b) =>
{
    return a-b;
});
console.log(numbers)

//ascending order
const number = [6,2,9,4];
numbers.sort((a,b) =>
{
    return b-a;
});
console.log(number);

//example

const product = 
[
  {pIb: "p1" , pName: "PenDrive", pPrice: 3000},
  {pIb: "p4" , pName: "Hardisk", pPrice: 50000},
  {pIb: "p2" , pName: "Pen", pPrice: 10},
  {pIb: "p3" , pName: "Book", pPrice: 100},
]
product.sort((a,b) => 
{
 return a.pPrice-b.pPrice
});
console.log(product);
