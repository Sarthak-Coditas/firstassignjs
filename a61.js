//find method
const names = ["Ray","Sahil","Shubham"];
function length(string)
{
 return names.length === 3;   
}

const ans = names.find(length);
console.log(ans);

//example

const details = 
[
  {userId: 1 , userName:"Sarthak"},
  {userId: 2 , userName:"Sahil"},
  {userId: 3 , userName:"Shubham"},
]

const ans1 = details.find((details) => details.userId ===3);
console.log(ans1);
