//every method
//eg : every method can be used to find whether all elemenrs present in array are even or not
//all cases must satisfy to obtain the true value
//every checks each element in array

//const numbers = [2,4,6,8];
//const ans = numbers.every((number) => number%2===0);
//console.log(ans);

//example
const users = 
[
  {firstName: "Sarthak", age:23, salary:40000},
  {firstName: "Sarthug", age:24, salary:30000},
  {firstName: "Sahil", age:25 , salary:20000},
]

const ans1 = users.every((locate) => locate.salary < 50000 );
console.log(ans1);