//splice method
//used to delete or add something in the array
//start,delete,inser

//deleting element
const arr1 = ["item1","item2","item3"];
arr1.splice(0,1);
console.log(arr1);

//inserting element
const arr2 = ["item4","item5","item6"];
arr2.splice(1,0,"inserted");
console.log(arr2);

//inserting and deleting simultaneously
const arr3 = ["item1","item2","item3"];
const deleted = arr3.splice(1,2,"newItem1","newItem2");
console.log(arr3);
console.log("deleted item : ",deleted)