//iterables and array like objects

//iterables
//iterables is generally where we can use for off loop
//string and arrays are iterable

const firstName = "Sarthak";
for(let char of firstName)
{
    console.log(char);
}

//array like objects
//array like objects has its own length property
//we can access it with the help of index
//string is the best example

const SecondName = "Sarthak";
console.log(SecondName.length);
console.log(SecondName[5]);
