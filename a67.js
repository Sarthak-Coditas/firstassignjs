//sets
// sets does not allow duplicate elements
const numbers =new Set([1,2,4,4]);
console.log(numbers);

// sets deos not allow index based access
const numberss =new Set([1,2,4,4]);
console.log(numberss);
console.log(numberss[3]); //output = undefined

// in sets order can be off any type
const newNum = new Set([2,4,3]);
console.log(newNum);

// sets can store any data types
const newArray = new Set([2,4,"Sarthak"]);
console.log(newArray);

// same array can be stored as it as different addresses
const sameArray = new Set();
sameArray.add(1);
sameArray.add(9);
sameArray.add([1,2,3]);
sameArray.add([1,2,3]);
console.log(sameArray);

// sets should be only used in case of unique values or elements
