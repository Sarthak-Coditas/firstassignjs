//maps objects
 const person1 = 
{
    id: 1,
    name: "Sarthak",
}

const person2 = 
{
    id: 2,
    name: "Priya",
}

const moreInfo = new Map();
moreInfo.set(person1, {age:22 , gender : "Male"});
moreInfo.set(person2, {age:21 , gender : "Female"});

console.log(person2.id);
console.log(moreInfo.get(person1).gender);
console.log(moreInfo.get(person2).gender);
