//cloning of objects

const info = 
{
    name: "Sarthak",
    age: 22,
}

const info1 = info;
//if we add a new element in info the same element will be added for info1
info.gender = "Male";
console.log(info1);

//cloning allows us to change the orginal block and keeping the duplicate one the same

const details = 
{
    name: "Sarthak",
    age: 22,
}
const newDetails = {...details};
details.gender = "MALE";
console.log(details);
console.log(newDetails);

