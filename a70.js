//optional chaining
//This is basically used when we try to access the objects values which are not even declred

//1
const person = 
{
 firstName: "Sarthak",
}

console.log(person.firstName);

//2
const person1 = 
{
 //firstNamee: "Sarthak",
}

console.log(person?.firstNamee);

//. gives result as error
//?. gives result as undefined