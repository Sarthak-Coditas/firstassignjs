//methods
// methods are functions inside objects

function personDetails() 
{
    console.log(`name is ${this.Name} and his age is ${this.id}`);

}

const person1 =
{
    Name: "Sarthak",
    id : 11103,
    about: personDetails,
}
const person2 =
{
    Name: "Sahil",
    id : 11173,
    about: personDetails,
}

person1.about();
person2.about();