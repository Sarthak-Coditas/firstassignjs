//call,apply and bind methods

//call
function info(hobby, favF1Driver)
{
    console.log(this.firstName, this.age,hobby,favF1Driver);
}
const user1 = 
{
  firstName: "Sarthak",
  age: 22,
}
const user2 = 
{
  firstName: "Sahil",
  age: 23,
}
//info.call(user2,"Motorsport", "Lewis" );

//apply
//only difference between call and apply is

//info.call(user2,"Motorsport", "Lewis" );
//info.apply(user2,["Motorsport", "Lewis"] );

//bind functions
//bind returns functions

const ans = info.bind(user2,"Motorsport", "Lewis");
ans();
