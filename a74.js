//binding example
const user1 = 
{
  firstName: "Sarthak",
  age: 22,
  about: function()
  {
    console.log(this.firstName, this.age);
  }
}

const ans = user1.about.bind(user1);
ans();