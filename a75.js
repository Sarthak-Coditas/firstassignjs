//arrow functions
//arrow functions dont have "this" in it
//arrow functions takes "this" from its surrounding
//"this" in arrow functions cannot be changed

const user1 = 
{
  firstName: "Sarthak",
  age: 22,
  about:() =>
  {
    console.log(this.firstName, this.age);
  }
}

user1.about(user1);
