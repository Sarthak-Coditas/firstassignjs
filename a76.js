// short syntax
//1

const user1 = 
{
  firstName: "Sarthak",
  age: 22,
  about:() =>
  {
    console.log(this.firstName, this.age);
  }
}

user1.about(user1);

//2
const user2 = 
{
  firstName: "Sarthak",
  age: 22,
  about()
  {
    console.log(this.firstName, this.age);
  }
}

user1.about(user1);