const userMethods=
{
about : function() 
 {
    return `${this.firstName} is ${this.age} years old`;

 },
 is18 : function()
 {
    return this.age >= 18;
 }
}

 function createUser(firstName , lastName , email , age , address)
{
 const user = {};
 user.firstName = firstName;
 user.lastName = lastName;
 user.email = email;
 user.age = age;
 user.address = address;
 user.about = userMethods.about;
 user.is18 = userMethods.is18;
 return user;
}
const user1 = createUser('Sarthak' , 'Naik' , 'sarthak@gmail.com' , 22 , "Panvel");
const user2 = createUser('Sahil' , 'Katle' , 'sahil@gmail.com' , 22 , "Panvel");
const user3 = createUser('Shubham' , 'Thakur' , 'shubham@gmail.com' , 22 , "Panvel");
console.log(user1.about());
console.log(user2.about());
console.log(user3.about());