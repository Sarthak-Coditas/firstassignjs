//prototype
//only functions provides prototypes
//functions provide useful properties
function hello()
{
    console.log("Hello World");
}

//we can add our own properties
hello.myOwnPrototype = "Very unique value";
console.log(hello.myOwnPrototype);

hello.prototype.abc = "abc";
hello.prototype.mno = "mno";
hello.prototype.sing = function()
{
    return "hello hello";
};
console.log(hello.prototype.sing());

