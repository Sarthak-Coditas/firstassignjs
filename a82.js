//using prototype
function createUser(firstName , lastName , email , age , address)
{
 const user = Object.create(createUser.prototype);
 user.firstName = firstName;
 user.lastName = lastName;
 user.email = email;
 user.age = age;
  user.address = address;
 return user;
}

createUser.prototype.about = function() 
{
   return `${this.firstName} is ${this.age} years old`;

}
createUser.prototype.is18 = function()
{
   return this.age >= 18;
}
createUser.prototype.sing = function()
{
   return `hello hello hello`;
}


const user1 = createUser('Sarthak' , 'Naik' , 'sarthak@gmail.com' , 22 , "Panvel");
const user2 = createUser('Sahil' , 'Katle' , 'sahil@gmail.com' , 22 , "Panvel");
const user3 = createUser('Shubham' , 'Thakur' , 'shubham@gmail.com' , 22 , "Panvel");
console.log(user1);
console.log(user1.is18());