//new keyword
function CreateUser(firstName , lastName , email , age , address)
{
 //const user = Object.create(createUser.prototype);
 this.firstName = firstName;
 this.lastName = lastName;
 this.email = email;
 this.age = age;
 this.address = address;
 return this;
}

CreateUser.prototype.about = function() 
{
   return `${this.firstName} is ${this.age} years old`;

}
CreateUser.prototype.is18 = function()
{
   return this.age >= 18;
}
CreateUser.prototype.sing = function()
{
   return `hello hello hello`;
}


const user1 = new CreateUser('Sarthak' , 'Naik' , 'sarthak@gmail.com' , 22 , "Panvel");
const user2 = new CreateUser('Sahil' , 'Katle' , 'sahil@gmail.com' , 22 , "Panvel");
const user3 = new CreateUser('Shubham' , 'Thakur' , 'shubham@gmail.com' , 22 , "Panvel");
console.log(user1);
console.log(user1.is18());