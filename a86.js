//class keyword
class CreateUser{
    constructor(firstName, lastName, email, age, address){
        console.log("constructor called");
        this.firstName = firstName
        this.lastName = lastName;
        this.email = email;
        this.age = age;
        this.address = address;
    }
    about(){
        console.log(this.firstName, this.age);
    }
    is18(){
        return this.age >= 18;
    }
    sing(){
        return 'As it Was';
    }
}

const user1 = new CreateUser("Sarthak", "Naik", 'sarthak@example.com', 22, "Panvel");
console.log(user1.about());