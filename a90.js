//getters and setters

// getter and setters

class Person{
    constructor(firstName, lastName, age){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }
    get fullName(){
        return `${this.firstName} ${this.lastname}.`;
    }
    set fullName(fullName){
        const [firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastname = lastName;
    }
}

const person1 = new Person("Sarthak", "Naik", 22);
// console.log(person1.fullName);
console.log(person1.firstName);
console.log(person1.lastName);
person1.fullName = "Lewis Hamilton";
console.log(person1.fullName);